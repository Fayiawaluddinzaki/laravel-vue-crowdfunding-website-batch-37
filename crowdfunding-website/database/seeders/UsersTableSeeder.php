<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('jp_JP');

        // for($i=1; $i<=30; $i++){
            User::create([
                'email' => $faker->email,
                'name' => $faker->name,
                'password' => Hash::make('usermakanmakan'),
                'roles_id' => 'd34ff443-6296-44e4-8e88-78fbf37b8857',
                'photo_profile' => $faker->imageUrl(300, 300, 'people'),
            ]);
        // }
    }
}
