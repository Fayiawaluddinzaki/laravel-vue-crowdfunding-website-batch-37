<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Emailverifikasi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
        $user = auth()->user();
        if ($user->password != '' && $user->email_verified_at != '') {
            return $next($request);
            // return response()->json(['message' => 'success'], 200);
            // return response()->redirectToRoute('home');
        }

        return response()->json([
            'message' => 'Silahkan verifikasi email anda'
        ], 401);
    }
}
