<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otp_codes extends Model
{
    use HasFactory;

    protected $fillable = ['otp', 'users_id', 'valid_until'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id');
    }
}
