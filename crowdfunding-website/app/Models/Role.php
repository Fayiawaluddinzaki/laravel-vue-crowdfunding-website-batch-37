<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
// use app\Traints\UsersUuid;

class Role extends Model
{
    use HasFactory , HasApiTokens , Notifiable;

    protected $fillable = ['name'];

    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
           if(empty($model->{$model->getKeyName()})){
               $model->{$model->getKeyName()} = Str::uuid();
           }
        });

    }


    public function users()
    {
        return $this->hasMany('App\Models\User','roles_id');
    }
}
