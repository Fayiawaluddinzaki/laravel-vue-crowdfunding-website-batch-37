<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
// use app\Traints\UsersUuid;
use Tymon\JWTAuth\Contracts\JWTSubject;
use app\Models\Role;
use app\Models\Otp_codes;
use carbon\carbon;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'photo_profile',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
           if(empty($model->{$model->getKeyName()})){
               $model->{$model->getKeyName()} = Str::uuid();
           }
        });

    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role','foreign_key');
    }

    public function otp_code()
    {
        return $this->hasOne('App\Models\Otp_codes');
    }

    public function getjwt()
    {
        return [
            'token' => $this->createToken('Laravel Password Grant Client')->accessToken,
            'user' => $this,
        ];
    }

    public function getrole()
    {
        $role = Role::where('name','user')->first();
        return $role->id;
    }

    public function generate_otp_code()
    {
        do{
            $random = mt_rand(100000, 999999);
            $check = Otp_codes::where('otp',$random)->first();
        }while($check);

        $now = Carbon::now();
        $otp = Otp_codes::updateOrCreate(
            ['user_id' => $this->id],
            ['otp' => $random, 'valid_until' => $now->addMinutes(5)]
        );
    }

}
